package app;

public class Entrada {

    public Articulo articulo;
    private int canti;
    private double importe;

    public Entrada(Articulo a, int c){

        articulo = a;
        canti = c;
        importe = articulo.precio * canti;
    }

    public void setCati(int c){

        canti = Math.abs(c);
        importe = c * articulo.precio;
    }

    public void increCati(){

        canti++;
        importe = canti * articulo.precio;
    }

    public void decreCati(){

        canti--;
        importe = canti * articulo.precio;
    }


    public double getImporte(){

        return importe;
    }
}
