package app;

import java.util.ArrayList;
import java.util.HashMap;

class Carrito {

    public HashMap<Integer, Entrada> lista = new HashMap<>();


    public int getCountArtis(){

        return lista.size();
    }

    public double getTotal(){

        double r = 0.0;

        for ( Entrada e : lista.values() )
            r += e.getImporte();

        return r;
    }


    public void agregaUnoMas(Articulo a){

        Entrada e = lista.get(a.id);

        if(e == null){
            e = new Entrada(a, 1);
            lista.put(e.articulo.id, e);
        }
        else
            e.increCati();


        System.out.println("");
        //revisar si el articulo esta ya en la lista
        //si esta actualizo la cantidad
        //si no agregao el arti a la lista
    }


    public void addArti(int c, Articulo a){

        Entrada e = lista.get(a.id);

        if(e == null){
            e = new Entrada(a, c);
            lista.put(e.articulo.id, e);
        }
        else
            e.setCati(c);

        //revisar si el articulo esta ya en la lista
        //si esta actualizo la cantidad
        //si no agregao el arti a la lista
    }


    public void updateArti(int c, Articulo a){

        addArti(c, a);
    }

    public void delete(Articulo a){

        lista.remove(a.id);
    }



    public ArrayList<Entrada> getLista(){

        ArrayList<Entrada> lista2 = new ArrayList<>();

        for (Entrada e : lista.values()) {

            lista2.add(e);
        }

        return lista2;
    }




}
